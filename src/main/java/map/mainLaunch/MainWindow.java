package map.mainLaunch;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainWindow extends Application{
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        //ArcGISRuntimeEnvironment.setInstallDirectory("100.11.2");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/map/mainLaunch/MainWindow.fxml"));

        Parent root = loader.load();
        stage.setTitle("MapControl");
        stage.setScene(new Scene(root, 600, 600));
        stage.show();
    }
}
