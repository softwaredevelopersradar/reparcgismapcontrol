package map.control;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class MapControl extends AnchorPane {
    MapController controller;

    public MapControl() {
        super();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("map/control/MapWindow"));
            controller = new MapController();
            loader.setController(controller);
            Node node = loader.load();
            this.getChildren().add(node);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
